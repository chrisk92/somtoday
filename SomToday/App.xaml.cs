﻿using System;
using System.Text.RegularExpressions;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

using SomToday.Services;
using SomToday.SomToday;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;

namespace SomToday
{
    public sealed partial class App : Application
    {
        private Lazy<ActivationService> _activationService;

        private ActivationService ActivationService
        {
            get { return _activationService.Value; }
        }

        public App()
        {
            InitializeComponent();

            // TODO WTS: Add your app in the app center and set your secret here. More at https://docs.microsoft.com/en-us/appcenter/sdk/getting-started/uwp
            AppCenter.Start("{Your App Secret}", typeof(Analytics), typeof(Crashes));

            // Deferred execution until used. Check https://msdn.microsoft.com/library/dd642331(v=vs.110).aspx for further info on Lazy<T> class.
            _activationService = new Lazy<ActivationService>(CreateActivationService);
        }

        protected override async void OnLaunched(LaunchActivatedEventArgs args)
        {
            if (!args.PrelaunchActivated)
            {
                Windows.Storage.ApplicationDataContainer localSettings =
         Windows.Storage.ApplicationData.Current.LocalSettings;
                try
                {
                    string username = localSettings.Values["username"].ToString();
                    string password = localSettings.Values["password"].ToString();
                    if (username == null)
                    {
                        UserData.Person = null;
                    }
                    else
                    {
                        string uuid = localSettings.Values["school"].ToString();
                        if (uuid == null)
                        {
                            UserData.Person = null;
                        }
                        else
                        {

                            SomToday.oAuth.UserProfileModel userProfileModel = await SomToday.AuthHandler.AuthUser(username, password, uuid);
                            SomToday.oAuth.UserProfileModel auth = userProfileModel;
                            if (auth != null)
                            {

                                SomToday.UserData.Person = auth.Persoon;
                                SomToday.UserData.Link = auth.Links;
                                //Means user authenticated, settings are stored so we can move to the next page.
                                Regex initials = new Regex(@"(\b[a-zA-Z])[a-zA-Z]* ?");
                                string init = initials.Replace(SomToday.UserData.Person.Roepnaam + " " + SomToday.UserData.Person.Achternaam, "$1");
                                //Init = "JD"
                                UserData.init = init;
                            }
                        }
                    }
                }
                catch (Exception e1)
                {
                    UserData.Person = null;
                }

                await ActivationService.ActivateAsync(args);
            }
        }

        protected override async void OnActivated(IActivatedEventArgs args)
        {
            await ActivationService.ActivateAsync(args);
        }

        private ActivationService CreateActivationService()
        {
            return new ActivationService(this, typeof(ViewModels.MainViewModel), new Lazy<UIElement>(CreateShell));
        }

        private UIElement CreateShell()
        {
            return new Views.ShellPage();
        }
    }
}
