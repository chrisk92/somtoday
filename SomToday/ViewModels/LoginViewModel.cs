﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using SomToday.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SomToday.ViewModels
{
    public class Documentation
    {
        public string Title { get; set; }
        public Uri Url { get; set; }
    }

    public class LoginViewModel :ViewModelBase
    {
        public static NavigationServiceEx NavigationService => ViewModelLocator.Current.NavigationService;

        public RelayCommand AuthUserCommand { get; private set; }
        public ObservableCollection<Documentation> RelatedContent
        {
            get { return relatedContent; }
            set { Set(ref relatedContent, value); }
        }
        private ObservableCollection<Documentation> relatedContent = new ObservableCollection<Documentation>();
        private string _usernameBox;
        public string UsernameBox
        {
            get { return _usernameBox; }
            set { Set(ref _usernameBox, value); }
        }
        private string _pwdBox;
        public string PwdBox
        {
            get { return _pwdBox; }
            set { Set(ref _pwdBox, value); }
        }
        private string _schoolGUID;
        public string schoolGUID
        {
            get { return _schoolGUID; }
            set { Set(ref _schoolGUID, value); }
        }
        
        public LoginViewModel()
        {
            this.AuthUserCommand = new RelayCommand(this.AuthUser, CanSignIn);
        }
        public bool CanSignIn()
        {
          if(PwdBox == string.Empty && UsernameBox == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public async void AuthUser()
        {
            var auth = await SomToday.AuthHandler.AuthUser(UsernameBox, PwdBox, schoolGUID);
            if(auth != null)
            {
                SomToday.UserData.Person = auth.Persoon;
                SomToday.UserData.Link = auth.Links;
                //Means user authenticated, settings are stored so we can move to the next page.
                Regex initials = new Regex(@"(\b[a-zA-Z])[a-zA-Z]* ?");
                string init = initials.Replace(SomToday.UserData.Person.Roepnaam + " " + SomToday.UserData.Person.Achternaam, "$1");
                ShellViewModel.Instance.HasUser = true;
                ShellViewModel.Instance.Inits = init;
                ShellViewModel.Instance.Name = auth.Persoon.Roepnaam + " " + auth.Persoon.Achternaam;
                NavigationService.Navigate("SomToday.ViewModels.MainViewModel");
            }
        }
    }
}
