﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Input;

using CommonServiceLocator;

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

using SomToday.Helpers;
using SomToday.Services;
using SomToday.SomToday;
using SomToday.SomToday.oAuth;
using SomToday.Views;
using Windows.Security.Credentials;
using Windows.Storage;
using Windows.System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
using WinUI = Microsoft.UI.Xaml.Controls;

namespace SomToday.ViewModels
{
    
    public class ShellViewModel : ViewModelBase, INotifyPropertyChanged
    {
        const int ElementNotFound = unchecked((int)0x80070490);
        private readonly KeyboardAccelerator _altLeftKeyboardAccelerator = BuildKeyboardAccelerator(VirtualKey.Left, VirtualKeyModifiers.Menu);
        private readonly KeyboardAccelerator _backKeyboardAccelerator = BuildKeyboardAccelerator(VirtualKey.GoBack);
        private Windows.Storage.ApplicationDataCompositeValue composite =
    new Windows.Storage.ApplicationDataCompositeValue();
        private bool _isBackEnabled;
        private bool hasUser;
        private WinUI.NavigationViewItem _selected;
        private Persoon _currentUser;
        private IList<KeyboardAccelerator> _keyboardAccelerators;
        private WinUI.NavigationView _navigationView;

        private string _name;
        private string _inits;
        private ICommand _loadedCommand;
        private string _headerTitle;
        private ICommand _itemInvokedCommand;
        private ICommand _itemTappedCommand;

        public WinUI.NavigationViewItem Selected
        {
            get { return _selected; }
            set { Set(ref _selected, value); }
        }

        public string Name
        {
            get { return _name; }
            set { Set(ref _name, value); }

        }
        public string Inits
        {
            get { return _inits; }
            set { Set(ref _inits, value); }

        }
        public string HeaderTitle
        {
            get { return _headerTitle; }
            set { Set(ref _headerTitle, value); }

        }
        public bool IsBackEnabled
        {
            get { return _isBackEnabled; }
            set { Set(ref _isBackEnabled, value); }
        }
        public Persoon CurrentUser
        {
            get { return _currentUser; }
            set { Set(ref _currentUser, value); }
        }

        public static NavigationServiceEx NavigationService => ViewModelLocator.Current.NavigationService;

        public bool HasUser
        {
            get { return hasUser; }
            set { Set(ref hasUser, value); }
        }
        public ICommand LoadedCommand => _loadedCommand ?? (_loadedCommand = new RelayCommand(OnLoaded));
        public ICommand ItemInvokedCommand => _itemInvokedCommand ?? (_itemInvokedCommand = new RelayCommand<WinUI.NavigationViewItemInvokedEventArgs>(OnItemInvoked));
        public ICommand ItemTappedCommand => _itemTappedCommand ?? (_itemTappedCommand = new RelayCommand<WinUI.NavigationViewItem>(OnTapInvoked));
        public static ShellViewModel Instance { get; private set; }

        public ShellViewModel()
        {
            Instance = this;
            
        }
 

        public void Initialize(Frame frame, WinUI.NavigationView navigationView, IList<KeyboardAccelerator> keyboardAccelerators)
        {
            _navigationView = navigationView;
            _keyboardAccelerators = keyboardAccelerators;
            NavigationService.Frame = frame;
            NavigationService.Navigated += Frame_Navigated;
            _navigationView.BackRequested += OnBackRequested;          
            navigationView.SelectedItem = navigationView.MenuItems[0];
            if (UserData.Person == null)
            {
                HasUser = false;
            }
            else
            {
                CurrentUser = UserData.Person;
                HasUser = true;
                Inits = UserData.init;
                Name = CurrentUser.Roepnaam + " " + CurrentUser.Achternaam;
            }
        }

        private void OnLoaded()
        {
            _keyboardAccelerators.Add(_altLeftKeyboardAccelerator);
            _keyboardAccelerators.Add(_backKeyboardAccelerator);

        }


        private void OnTapInvoked(WinUI.NavigationViewItem args)
        {
            if ((string)args.Content == "Sign out")
            {
                HasUser = false;
                Windows.Storage.ApplicationDataContainer localSettings =
  Windows.Storage.ApplicationData.Current.LocalSettings;
                localSettings.Values["username"] = null;
                localSettings.Values["password"] = null;
                localSettings.Values["school"] = null;
                UserData.Person = null;
                CurrentUser = null;
                UserData.init = "";
                Inits = null;
                Name = "Not signed in";
                NavigationService.Navigate(typeof(LoginViewModel).FullName);
            }
        }

        private void OnItemInvoked(WinUI.NavigationViewItemInvokedEventArgs args)
        {
            if (args.IsSettingsInvoked)
            {
                NavigationService.Navigate(typeof(SettingsViewModel).FullName);
                return;
            }

            var item = _navigationView.MenuItems
                            .OfType<WinUI.NavigationViewItem>()
                            .First(menuItem => (string)menuItem.Content == (string)args.InvokedItem);
            var pageKey = item.GetValue(NavHelper.NavigateToProperty) as string;
            HeaderTitle = item.Content.ToString();
            NavigationService.Navigate(pageKey);
        }

        private void OnBackRequested(WinUI.NavigationView sender, WinUI.NavigationViewBackRequestedEventArgs args)
        {
            NavigationService.GoBack();
        }

        private void Frame_Navigated(object sender, NavigationEventArgs e)
        {
            IsBackEnabled = NavigationService.CanGoBack;
            if (e.SourcePageType == typeof(SettingsPage))
            {
               var stItem = _navigationView.SettingsItem as WinUI.NavigationViewItem;
                Selected = stItem;
                HeaderTitle = stItem.Content.ToString();
                return;
            }

           var item = _navigationView.MenuItems
                            .OfType<WinUI.NavigationViewItem>()
                            .FirstOrDefault(menuItem => IsMenuItemForPageType(menuItem, e.SourcePageType));
            Selected = item;
            HeaderTitle = item.Content.ToString();
        }

        private bool IsMenuItemForPageType(WinUI.NavigationViewItem menuItem, Type sourcePageType)
        {
            var navigatedPageKey = NavigationService.GetNameOfRegisteredPage(sourcePageType);
            var pageKey = menuItem.GetValue(NavHelper.NavigateToProperty) as string;
            return pageKey == navigatedPageKey;
        }

        private static KeyboardAccelerator BuildKeyboardAccelerator(VirtualKey key, VirtualKeyModifiers? modifiers = null)
        {
            var keyboardAccelerator = new KeyboardAccelerator() { Key = key };
            if (modifiers.HasValue)
            {
                keyboardAccelerator.Modifiers = modifiers.Value;
            }

            keyboardAccelerator.Invoked += OnKeyboardAcceleratorInvoked;
            return keyboardAccelerator;
        }

        private static void OnKeyboardAcceleratorInvoked(KeyboardAccelerator sender, KeyboardAcceleratorInvokedEventArgs args)
        {
            var result = NavigationService.GoBack();
            args.Handled = result;
        }
    }
}
