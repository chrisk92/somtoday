﻿using System;
using System.Diagnostics;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Windows.UI.Xaml.Controls.Primitives;

namespace SomToday.ViewModels
{
    public class TestViewModel : ViewModelBase
    {
        private bool isVisible;

        public TestViewModel()
        {
        }


        public bool IsVis
        {
            get { return isVisible; }
            set { Set(ref isVisible, value); }
        }

    }
}
