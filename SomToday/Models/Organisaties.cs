﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SomToday.Models
{

    public partial class Organisaties
    {
        [JsonProperty("instellingen")]
        public Instellingen[] Instellingen { get; set; }
    }

    public partial class Instellingen
    {
        [JsonProperty("uuid")]
        public Guid Uuid { get; set; }

        [JsonProperty("naam")]
        public string Naam { get; set; }

        [JsonProperty("plaats")]
        public string Plaats { get; set; }
    }

    public partial class Organisaties
    {
        public static Organisaties[] FromJson(string json) => JsonConvert.DeserializeObject<Organisaties[]>(json, Converter.Settings);
    }

   
}
