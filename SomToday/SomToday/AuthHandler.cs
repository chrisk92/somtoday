﻿using SomToday.Models;
using SomToday.SomToday.oAuth;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Credentials;
using Windows.UI.Popups;

namespace SomToday.SomToday
{
   public static class AuthHandler
    {
        //SomToday Client ID & Secret
        private static string base64Creds = Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(string.Format("{0}:{1}", "D50E0C06-32D1-4B41-A137-A9A850C892C2", "vDdWdKwPNaPCyhCDhaCnNeydyLxSGNJX")));
        private static HttpRequestMessage request = new System.Net.Http.HttpRequestMessage(System.Net.Http.HttpMethod.Post, "");
        private static HttpClient client = new System.Net.Http.HttpClient();
        private static Windows.Storage.ApplicationDataContainer localSettings =
    Windows.Storage.ApplicationData.Current.LocalSettings;
        public static async Task<UserProfileModel> GetUser(Welcome user)
        {
            //API Endpoint uses Bearer token authentican, thus:
            //https://TENANT-api.somtoday.nl/rest/v1/account/me with header: Bearer{0} AuthKey and accept: application/json
            string baseurl = user.SomtodayApiUrl.ToString();
            string apiEndpoint = "/rest/v1/account/me";
            //Init httpClient
            HttpClient httpClient = new HttpClient();
            //Set headers
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", user.AccessToken.ToString());
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string loginUrl = baseurl + apiEndpoint;
            //Execute function and read
            HttpResponseMessage response = await httpClient.GetAsync(loginUrl);
            string UserResponse = await response.Content.ReadAsStringAsync();
            //Deserialize JSON string and put in model
            var userProfileModel = UserProfileModel.FromJson(UserResponse);
            localSettings.Values["authKey"] = user.AccessToken;
            localSettings.Values["authKey"] = user.RefreshToken;
            Debug.WriteLine(userProfileModel.Persoon.Roepnaam);
            return userProfileModel;
        }

        public static async Task<UserProfileModel> AuthUser(string username, string password, string uuid)
        {
            try
            {
                //Form encoded data
                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("username", uuid + @"\" + username));
                keyValues.Add(new KeyValuePair<string, string>("password", password));
                keyValues.Add(new KeyValuePair<string, string>("scope", "openid"));
                keyValues.Add(new KeyValuePair<string, string>("grant_type", "password"));

                request.Content = new FormUrlEncodedContent(keyValues);

                var client = new System.Net.Http.HttpClient();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64Creds);
                var headers = client.DefaultRequestHeaders;
                //The safe way to add a header value is to use the TryParseAdd method and verify the return value is true,
                //especially if the header value is coming from user input.
                string header = "ie";
                if (!headers.UserAgent.TryParseAdd(header))
                {
                    throw new Exception("Invalid header value: " + header);
                }

                header = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)";
                if (!headers.UserAgent.TryParseAdd(header))
                {
                    throw new Exception("Invalid header value: " + header);
                }

                var response = await client.PostAsync("https://somtoday.nl/oauth2/token", request.Content);
                string jsonResponse = await response.Content.ReadAsStringAsync();
                if (jsonResponse.Contains("error"))
                {
                    MessageDialog incDetals = new MessageDialog("Incorrect details, please check your password and/or username.", "A fatal error has occured.");
                    await incDetals.ShowAsync();
                    return null;
                }
                else
                {
                    var welcome = Welcome.FromJson(jsonResponse);
                    localSettings.Values["username"] = username;
                    localSettings.Values["password"] = password;

                    localSettings.Values["school"] = uuid;
                    var result = await GetUser(welcome);
                    return result;
                }
            }
            catch (Exception err)
            {
                MessageDialog ftlrror = new MessageDialog(err.Message + ", please inform the developer about this issue and provide them with the error code.", "A fatal error has occured: " + err.HResult.ToString());
                await ftlrror.ShowAsync();
                return null;
            }
        }
    }
}
