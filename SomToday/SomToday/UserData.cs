﻿using SomToday.SomToday.oAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomToday.SomToday
{
    public static class UserData
    {
        private static Link[] _link { get; set; }
        private static string _init { get; set; }
        private static Persoon _person { get; set; }
        
        public static Persoon Person
        {
            get { return _person; }
            set { _person = value; }
        }
        public static string init
        {
            get { return _init; }
            set { _init = value; }
        }

        public static Link[] Link
        {
            get { return _link; }
            set { _link = value; }
        }

    }
}
