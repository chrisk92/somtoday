﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomToday.SomToday.oAuth
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class UserProfileModel
    {
        [JsonProperty("links")]
        public Link[] Links { get; set; }

        [JsonProperty("permissions")]
        public Permission[] Permissions { get; set; }

        [JsonProperty("additionalObjects")]
        public AdditionalObjects AdditionalObjects { get; set; }

        [JsonProperty("gebruikersnaam")]
        public string Gebruikersnaam { get; set; }

        [JsonProperty("accountPermissions")]
        public object[] AccountPermissions { get; set; }

        [JsonProperty("persoon")]
        public Persoon Persoon { get; set; }
    }

    public partial class AdditionalObjects
    {
    }

    public partial class Link
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("rel")]
        public string Rel { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("href")]
        public Uri Href { get; set; }
    }

    public partial class Permission
    {
        [JsonProperty("full")]
        public string Full { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("operations")]
        public string[] Operations { get; set; }

        [JsonProperty("instances")]
        public string[] Instances { get; set; }
    }

    public partial class Persoon
    {
        [JsonProperty("$type")]
        public string Type { get; set; }

        [JsonProperty("links")]
        public Link[] Links { get; set; }

        [JsonProperty("permissions")]
        public Permission[] Permissions { get; set; }

        [JsonProperty("additionalObjects")]
        public AdditionalObjects AdditionalObjects { get; set; }

        [JsonProperty("leerlingnummer")]
        public long Leerlingnummer { get; set; }

        [JsonProperty("roepnaam")]
        public string Roepnaam { get; set; }

        [JsonProperty("achternaam")]
        public string Achternaam { get; set; }
    }

    public partial class UserProfileModel
    {
        public static UserProfileModel FromJson(string json) => JsonConvert.DeserializeObject<UserProfileModel>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this UserProfileModel self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
