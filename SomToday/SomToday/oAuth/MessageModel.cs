﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomToday.SomToday.oAuth
{
    public class MessageModel
    {
        public ResponseMessages MessageType { get; set; }

        public string Content { get; set; }

    }
}
