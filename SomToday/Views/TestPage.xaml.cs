﻿using System;

using SomToday.ViewModels;

using Windows.UI.Xaml.Controls;

namespace SomToday.Views
{
    public sealed partial class TestPage : Page
    {
        private TestViewModel ViewModel
        {
            get { return ViewModelLocator.Current.TestViewModel; }
        }

        public TestPage()
        {
            InitializeComponent();
        }

        private void ToggleButton_Checked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {

        }
    }
}
