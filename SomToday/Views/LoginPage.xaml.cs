﻿using SomToday.Models;
using SomToday.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Numerics;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.UI.Composition;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Hosting;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SomToday.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginPage : Page
    {
        Compositor _compositor = Window.Current.Compositor;
        private SpringVector3NaturalMotionAnimation _springAnimation;
        private LoginViewModel ViewModel
        {
            get { return ViewModelLocator.Current.LoginViewModel; }
        }

        public LoginPage()
        {
            this.InitializeComponent();
          
            DataContext = ViewModel;
        }
        
  
        private void OnContentRootSizeChanged(object sender, SizeChangedEventArgs e)
        {
            string targetState = "NormalFrameContent";

            if ((contentColumn.ActualWidth) >= 1000)
            {
                targetState = "WideFrameContent";
            }

            VisualStateManager.GoToState(this, targetState, false);
        }
        private void element_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            UpdateSpringAnimation(1.2f);

            StartAnimationIfAPIPresent((sender as UIElement), _springAnimation);
        }
        private void UpdateSpringAnimation(float finalValue)
        {
            if (_springAnimation == null)
            {
                _springAnimation = _compositor.CreateSpringVector3Animation();
                _springAnimation.Target = "Scale";
            }

            _springAnimation.FinalValue = new Vector3(finalValue);
            _springAnimation.DampingRatio = GetDampingRatio();
            _springAnimation.Period = GetPeriod();
        }
        private void element_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            UpdateSpringAnimation(1f);

            StartAnimationIfAPIPresent((sender as UIElement), _springAnimation);
        }
        float GetDampingRatio()
        {
            return 0.6f;
        }

        TimeSpan GetPeriod()
        {
            return TimeSpan.FromMilliseconds(25);
        }

        private void StartAnimationIfAPIPresent(UIElement sender, Windows.UI.Composition.CompositionAnimation animation)
        {
            if (ApiInformation.IsApiContractPresent("Windows.Foundation.UniversalApiContract", 7))
            {
                (sender as UIElement).StartAnimation(animation);
            }
        }


        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            // Animate the translation of each button relative to the scale and translation of the button above.
            var anim = _compositor.CreateExpressionAnimation();
            anim.Expression = "(above.Scale.Y - 1) * 25 + above.Translation.Y % (25 * index)";
            anim.Target = "Translation.Y";

            // Animate the second button relative to the first.
            anim.SetExpressionReferenceParameter("above", userBox);
            anim.SetScalarParameter("index", 1);
            pwdText.StartAnimation(anim);

            // Animate the third button relative to the second.
            anim.SetExpressionReferenceParameter("above", pwdText);
            anim.SetScalarParameter("index", 2);
            pwdBox.StartAnimation(anim);

            // Animate the 4th button relative to the 3rd.
            anim.SetExpressionReferenceParameter("above", pwdBox);
            anim.SetScalarParameter("index", 3);
            btn1.StartAnimation(anim);


        }

        /// <summary>
        /// This event gets fired anytime the text in the TextBox gets updated.
        /// It is recommended to check the reason for the text changing by checking against args.Reason
        /// </summary>
        /// <param name="sender">The AutoSuggestBox whose text got changed.</param>
        /// <param name="args">The event arguments.</param>
        private async void Control2_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            //We only want to get results when it was a user typing,
            //otherwise we assume the value got filled in by TextMemberPath
            //or the handler for SuggestionChosen
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                var suggestions = await SearchControls(sender.Text);

                if (suggestions.Count > 0)
                    sender.ItemsSource = suggestions;
                else
                    sender.ItemsSource = new string[] { "No results found" };
            }
        }

        /// <summary>
        /// This event gets fired when:
        ///     * a user presses Enter while focus is in the TextBox
        ///     * a user clicks or tabs to and invokes the query button (defined using the QueryIcon API)
        ///     * a user presses selects (clicks/taps/presses Enter) a suggestion
        /// </summary>
        /// <param name="sender">The AutoSuggestBox that fired the event.</param>
        /// <param name="args">The args contain the QueryText, which is the text in the TextBox,
        /// and also ChosenSuggestion, which is only non-null when a user selects an item in the list.</param>
        private async void Control2_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            if (args.ChosenSuggestion != null && args.ChosenSuggestion is Instellingen)
            {
                //User selected an item, take an action
                SelectControl(args.ChosenSuggestion as Instellingen);
            }
            else if (!string.IsNullOrEmpty(args.QueryText))
            {
                //Do a fuzzy search based on the text
                var suggestions = await SearchControls(sender.Text);
                if (suggestions.Count > 0)
                {
                    SelectControl(suggestions.FirstOrDefault());
                }
            }
        }

        /// <summary>
        /// This event gets fired as the user keys through the list, or taps on a suggestion.
        /// This allows you to change the text in the TextBox to reflect the item in the list.
        /// Alternatively you can use TextMemberPath.
        /// </summary>
        /// <param name="sender">The AutoSuggestBox that fired the event.</param>
        /// <param name="args">The args contain SelectedItem, which contains the data item of the item that is currently highlighted.</param>
        private void Control2_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            //Don't autocomplete the TextBox when we are showing "no results"
            if (args.SelectedItem is Instellingen control)
            {
                sender.Text = control.Naam;
                guidSchool.Text = control.Uuid.ToString();
            }
        }

        /// <summary>
        /// This
        /// </summary>
        /// <param name="contact"></param>
        private void SelectControl(Instellingen control)
        {
            if (control != null)
            {

            }
        }

        private async Task<List<Instellingen>> SearchControls(string query)
        {
            var suggestions = new List<Instellingen>();
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync("https://servers.somtoday.nl/organisaties.json");
            var organisaties = Organisaties.FromJson(await response.Content.ReadAsStringAsync());

                var matchingItems = organisaties[0].Instellingen.Where(
                    item => item.Naam.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) >= 0);

                foreach (var item in matchingItems)
                {
                    suggestions.Add(item);
                }

            return suggestions.OrderByDescending(i => i.Naam.StartsWith(query, StringComparison.CurrentCultureIgnoreCase)).ThenBy(i => i.Naam).ToList();
        }
    }
}
